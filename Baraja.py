from Carta import Carta
import random
class Baraja:
    baraja=[]

    def __init__(self):
        self.rellenarBaraja()

    #Metodo para rellenar baraja con 48 cartas de 12 numeros y 4 palos
    def rellenarBaraja(self):
        palos = ["Bastos", "Espadas", "Copas", "Oros"]
        for x in range (4):
            for y in range (1,13):
                carta = Carta(y,palos[x])
                self.baraja.append(carta)


    def getBaraja(self):
        return self.baraja

    def getRandomCarta(self):
        randomNum = random.randrange(0,len(self.baraja))
        carta = self.baraja[randomNum]
        self.baraja.pop(randomNum)
        return carta

    def getNumCartas(self):
        return len(self.baraja)

