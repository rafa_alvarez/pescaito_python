class Carta:
    numero = ""
    palo =""

    #Constructor
    def __init__(self,numero,palo):
        self.numero = numero
        self.palo = palo

    #Methods for numero
    def getNumero(self):
        return self.numero
    def setNumero(self, numero):
        self.numero = numero

    #Methods for palo
    def getPalo(self):
        return self.palo
    def setPalo(self,palo):
        self.palo = palo
