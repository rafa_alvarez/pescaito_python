class Jugador:
    nombre = ""
    cartas = []
    puntuacion= 0

    def __init__(self,nombre,cartas):
        self.nombre = nombre
        self.cartas = cartas

    def setNombre(self,nombre):
        self.nombre = nombre
    def getNombre(self):
        return self.nombre

    def setCartas(self,cartas):
        self.cartas = cartas
    def getCartas(self):
        return self.cartas
    def robarCarta(self,index):
        self.cartas.pop(index)
    def añadirCarta(self,carta):
        self.cartas.append(carta)

    def setPuntuacion(self,puntuacion):
        self.puntuacion = puntuacion
    def getPuntiacion(self):
        return self.puntuacion
    def addPuntuacion(self):
        self.puntuacion += 1