from Baraja import Baraja
from Jugador import Jugador
from Movimiento import Movimiento
from os import system, name

class Partida:
    jugadores = []
    baraja = Baraja()
    movimientos = []

    def __init__(self,numJugadores):
        self.crearPartida(numJugadores)
        self.partida()

    #Funcion para borrar consola
    def clear(self):
        if name == 'nt':
            _ = system('cls')
        else:
            _ = system('clear')

    #Creamos jugadores y asignamos 4 cartas
    def crearPartida(self,numJugadores):
        for x in range (numJugadores):
            nombre = input("Introduce el nombre del jugador numero " + str(x+1) + ": ")
            cartas = []
            for i in range (4):
               cartas.append(self.baraja.getRandomCarta())

            jugador = Jugador(nombre, cartas)
            self.jugadores.append(jugador)

    #Programa principal
    def partida(self):
        indexJugador  = 0
        while(self.baraja.getNumCartas() > 0):
            movimiento = self.movimiento(self.jugadores[indexJugador])
            while(movimiento == True):
                movimiento = self.movimiento(self.jugadores[indexJugador])
            input("Presiona Enter para pasar de turno")
            self.clear()
            if(indexJugador < len(self.jugadores)-1):
                indexJugador = indexJugador + 1
            else:
                indexJugador = 0

    #El movimiento y retorna si ha sido bona pesca o no
    def movimiento(self,jugador):
        #Pedimos datos
        print("Es tu turno " + jugador.getNombre() + "!")
        self.printCartas(jugador)
        jugador = self.getJugadorByNombre(jugador.getNombre())
        nombreJugador = input("Introduce el nombre de la persona a la que deseas pedir la carta: ")
        numCarta = int(input("Introduce el numero de la carta (1-12) "))
        while(self.cartaInJugadorBaraja(jugador.getCartas(),numCarta) == False):
            print("Ups! Solo puedes pedir cartas que esten en tu baraja")
            numCarta = int(input("Introduce el numero de la carta (1-12) "))

        jugador2 = self.getJugadorByNombre(nombreJugador)
        #Ejecutamos la accion
        bonaPesca = True
        cartasRobadas = self.pedirCarta(jugador,jugador2,numCarta)
        if(cartasRobadas == 0):
            print("No has tenido suerte robas 1")
            cartaRobada = self.baraja.getRandomCarta()
            jugador.añadirCarta(cartaRobada)
            print("Has robado un " + str(cartaRobada.getNumero()) + " de " + cartaRobada.getPalo())
            bonaPesca = False
            if(cartaRobada.getNumero() == numCarta):
                bonaPesca = True
                print("Bona Pesca!!! Tienes otro turno")
        self.printCartas(jugador)
        movimiento = Movimiento(jugador,jugador2,numCarta,bonaPesca)
        self.movimientos.append(movimiento)
        return bonaPesca

    #Funcion para imprimir todas las cartas del jugador
    def printCartas(self,jugador):
        for i in jugador.getCartas():
            print(str(i.getNumero()) + " " + i.getPalo())

    #Devuelve un objeto Jugador a partir de un nombre
    def getJugadorByNombre(self,nombre):
        for i in self.jugadores:
            if(i.getNombre() == nombre):
                return i

    #Funcion de pedir una carta de un jugador a otro jugador
    def pedirCarta(self,jugador,jugador2,numCarta):
        contador = 0
        cartasJugador = jugador.getCartas()
        contadorJugador = 0
        cartas = jugador2.getCartas()
        for index,i in enumerate(cartas):
            if i.getNumero() == numCarta:
                jugador2.robarCarta(index)
                jugador.añadirCarta(i)
                contador +=1
        print(jugador2.getNombre() + " tenia una cantidad de " + str(contador) + " de la carta " + str(numCarta))
        indexs = []
        for i in cartasJugador:
            if i.getNumero == numCarta:
                contadorJugador += 1
                indexs.append(i)

        if contadorJugador == 4:
            print("tienes todas las cartas de este numero. DESCARTAS!")
            for i in indexs:
                jugador.robarCarta(indexs[i])
        return contador

    def cartaInJugadorBaraja(self,baraja,numero):
        includes = False
        for i in baraja:
            if(i.getNumero() == numero):
                includes = True

        return includes

